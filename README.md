# Logify96

Get notifications from system log

## What it does

 - play sound
 - give detailed explanations
 - notify instantly
 - determine the app
 - show in the corner of your screen
 - hide/mute info log entries
 - get Hidden Metadata from log
